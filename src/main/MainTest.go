package main
import (
	"fmt"
	"bufio"
	"os"
	"strings"
	"io/ioutil"
	"regexp"
)

var scanner = bufio.NewScanner(os.Stdin)
const USER_FILE string = "localBase.txt"

func main () {
	showNavigateText();
	controller();
 }

 func controller() {
	 for scanner.Scan() {
		 textFromConsole := strings.Split(scanner.Text(), " ")
		 switch textFromConsole[0] {
		 case "create":
			 if len(textFromConsole) == 4 && validatePhoneNumer(textFromConsole[1]) {
				 createNewContact(textFromConsole)
			 } else {
				 fmt.Println("Неверное использование команды")
				 fmt.Println("create [телефон в формате +380631891717] [имя] [фамилия]")
			 }
		 case "read":
			 listPhones()
		 case "update":
			 if len(textFromConsole) >= 5 && validatePhoneNumer(textFromConsole[1]) {
				 updatePhoneNumber(textFromConsole[1], textFromConsole[2], textFromConsole[3])
			 } else {
				 fmt.Println("Неверное использование команды")
				 fmt.Println("обновить запись - update [телефон в формате +380631891717] [новое имя] [новая фамилия]")
			 }
		 case "delete":
			 if len(textFromConsole) >= 5 && validatePhoneNumer(textFromConsole[1]) {
				 deletePhoneNumber(textFromConsole[1])
			 } else {
				 fmt.Println("Неверное использование команды")
				 fmt.Println("delete [телефон в формате +380631891717]")
			 }
		 case "start":
			 start()
		 case "exit":
			 os.Exit(0)
		 default:
			 fmt.Println("Неверная комманда")
			 fmt.Println("Введите start для начала работы")
		 }
	 }
 }

func createNewContact(textFromConsole [] string) {
	var phone = textFromConsole[1]
	var name = textFromConsole[2]
	var lastName = textFromConsole[3]

	file, err := os.OpenFile(USER_FILE, os.O_APPEND|os.O_WRONLY, 0600)
	defer file.Close()
	if err != nil {
		createFile(USER_FILE)
		createNewContact(textFromConsole)
		return
	}
	in, _ := ioutil.ReadFile(USER_FILE)
	lines := strings.Split(string(in), "\n")

	for _, line := range lines {
		if strings.Split(line, " ")[0] == phone {
			fmt.Println("Запись с таким номером уже существует")
			return
		}
	}
	str := phone + " " + name + " " + lastName + "\n"
	if _, err = file.WriteString(str); err != nil {
		fmt.Println("Запись в файл не удалась.")
		return
	}
	fmt.Println("Запись '" + phone + " " + name + " " + lastName + "' успешно создана")
}

func showNavigateText() {
	fmt.Println("Введите start для начала работы")
}

func createFile(name string) {
	_, err := os.Create(name)
	if err != nil {
		fmt.Println("Ошибка при создании файла, повторите снова")
		return
	}
}

func validatePhoneNumer(phone string) (isValid bool) {
	var validPhone = regexp.MustCompile("^[+][3][8][0][0-9\\s\\(\\)\\+]{9}$")
	if !validPhone.MatchString(phone) {
		fmt.Println("Номер телефона должен быть в формате +380631891717")
		return false
	}
	return true
}

func updatePhoneNumber(phone string, name string, lastName string) {
	in, _ := ioutil.ReadFile(USER_FILE)
	lines := strings.Split(string(in), "\n")
	for i, line := range lines {
		tmp := line
		lines[i] = phone + " " + name + " " + lastName
		out := strings.Join(lines, "\n")
		ioutil.WriteFile(USER_FILE, []byte(out), 0644)
		fmt.Println("Запись " + tmp + " обновлена")
		fmt.Println("Новые данные:  " + lines[i])
		return
	}
	fmt.Println("Номер " + phone + " не найден в базе")
}

func deletePhoneNumber(phone string) {
	in, _ := ioutil.ReadFile(USER_FILE)

	lines := strings.Split(string(in), "\n")
	for i, line := range lines {
		if strings.Split(line, " ")[0] == phone {
			lines[i] = lines[len(lines)-1]
			lines = lines[:len(lines)-1]
			fmt.Println("Номер " + phone + " удален успешно")
			out := strings.Join(lines, "\n")
			ioutil.WriteFile(USER_FILE, []byte(out), 0644)
			return
		}
	}
	fmt.Println("Номер " + phone + " не найден в базе")

}

func start() {
	fmt.Println("создать запись - create [телефон в формате +380631891717] [имя] [фамилия]")
	fmt.Println("вывести список записей - read")
	fmt.Println("удалить запись - delete [телефон в формате +380631891717]")
	fmt.Println("обновить запись - update [телефон в формате +380631891717] [новое имя] [новая фамилия]")
	fmt.Println("завершить программу - exit")
}

func listPhones() {
	in, _ := ioutil.ReadFile(USER_FILE)
	lines := strings.Split(string(in), "\n")
	fmt.Println("PHONE | NAME | LAST_NAME")
	for _, line := range lines {
		fmt.Println(line)
	}
}